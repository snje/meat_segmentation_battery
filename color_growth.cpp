#include <iostream>
#include <vector>
#include <algorithm>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/region_growing.h>
#include <pcl/segmentation/region_growing_rgb.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <stdio.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/ModelCoefficients.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

typedef pcl::PointXYZRGB PointType;


std::string input_path = "";
std::string output_path = "/tmp/output.pcd";

float smooth_thresh = 5 / 180.0 * M_PI;
int neighbors = 30;
float curve_thresh = 20.0;
float distance_thresh = 20;
float point_color_thresh = 6;
float region_color_thresh = 5;
bool visualization = false;

void handle_long_options(std::string name) {
  if (name == "point_color") {
    point_color_thresh = atof(optarg);
  } else if (name == "region_color") {
    region_color_thresh = atof(optarg);
  } else if (name == "distance") {
    distance_thresh = atof(optarg);
  } else if (name == "curve") {
    curve_thresh = atof(optarg);
  } else if (name == "smooth") {
    smooth_thresh = atof(optarg) / 180.0 * M_PI;;
  } else if (name == "output") {
    output_path = optarg;
  } else if (name == "visualize") {
    visualization = true;
  } else {
    printf("Unknown option %s\n", name.c_str());
  }
}

int main (int argc, char** argv)
{
  int c;
  int digit_optind = 0;
  while (1) {
       int this_option_optind = optind ? optind : 1;
       int option_index = 0;
       static struct option long_options[] = {
           {"point_color",     required_argument, 0,  0 },
           {"region_color",  required_argument,       0,  0 },
           {"distance",  required_argument, 0,  0 },
           {"curve", required_argument,       0,  0 },
           {"smooth",  required_argument, 0, 0},
           {"output",    required_argument, 0,  0 },
           {"visualize",    no_argument, 0,  0 },
           {0,         0,                 0,  0 }
       };

      c = getopt_long(argc, argv, "abc:d:012",
                long_options, &option_index);
       if (c == -1)
           break;

      switch (c) {
       case 0:
       /*
           printf("option %s", long_options[option_index].name);
           if (optarg)
               printf(" with arg %s", optarg);
           printf("\n");
           */
           handle_long_options(long_options[option_index].name);
           break;

      case '?':
           break;

      default:
           printf("?? getopt returned character code 0%o ??\n", c);
       }
   }

  if (optind < argc) {
    input_path = argv[optind];
   } else {
     printf("\nInput path must be supplied.\n");
     return -1;
   }

  pcl::PointCloud<PointType>::Ptr cloud_in (new pcl::PointCloud<PointType>);
  if ( pcl::io::loadPCDFile <PointType> (input_path, *cloud_in) == -1)
  {
    std::cout << "Cloud reading failed." << std::endl;
    return (-1);
  }
  pcl::PointCloud<PointType>::Ptr cloud (new pcl::PointCloud<PointType>);
  // Create the filtering object
  pcl::VoxelGrid<PointType> sor;
  sor.setInputCloud (cloud_in);
  sor.setLeafSize (0.002f, 0.002f, 0.002f);
  sor.filter (*cloud);

  pcl::search::Search<PointType>::Ptr tree = boost::shared_ptr<pcl::search::Search<PointType> > (new pcl::search::KdTree<PointType>);
  pcl::PointCloud <pcl::Normal>::Ptr normals (new pcl::PointCloud <pcl::Normal>);
  pcl::NormalEstimation<PointType, pcl::Normal> normal_estimator;
  normal_estimator.setSearchMethod (tree);
  normal_estimator.setInputCloud (cloud);
  int k = 25;
  normal_estimator.setKSearch (k);
  normal_estimator.compute (*normals);

  pcl::IndicesPtr indices (new std::vector <int>);
  pcl::PassThrough<PointType> pass;
  pass.setInputCloud (cloud);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (0.0, 1.0);
  pass.filter (*indices);

  pcl::RegionGrowingRGB<PointType, pcl::Normal> reg;
  reg.setMinClusterSize (50);
  reg.setMaxClusterSize (1000000);
  reg.setSearchMethod (tree);
  reg.setNumberOfNeighbours (neighbors);
  reg.setInputCloud (cloud);
  //reg.setIndices (indices);
  reg.setInputNormals (normals);
  reg.setSmoothnessThreshold (smooth_thresh);
  reg.setCurvatureThreshold (curve_thresh);
  reg.setDistanceThreshold (distance_thresh);
  reg.setPointColorThreshold (point_color_thresh);
  reg.setRegionColorThreshold (region_color_thresh);

  std::vector <pcl::PointIndices> clusters;
  reg.extract (clusters);


  auto max_cluster = clusters.begin();
  for (auto it = clusters.begin() + 1; it != clusters.end(); it++) {
    if (max_cluster->indices.size() < it->indices.size()) {
      max_cluster = it;
    }
  }
  // Create the filtering object
  pcl::ExtractIndices<PointType> extract;
  pcl::PointCloud<PointType>::Ptr cloud_final (new pcl::PointCloud<PointType>);
  // Extract the inliers
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
  *inliers = *max_cluster;
  extract.setInputCloud (cloud);
  extract.setIndices (inliers);
  extract.setNegative (false);
  extract.filter (*cloud_final);

  pcl::ExtractIndices<pcl::Normal> extract_normal;
  pcl::PointCloud <pcl::Normal>::Ptr normals_final (new pcl::PointCloud <pcl::Normal>);
  extract_normal.setInputCloud (normals);
  extract_normal.setIndices (inliers);
  extract_normal.setNegative (false);
  extract_normal.filter (*normals_final);


  if (visualization) {
    pcl::PointCloud <pcl::PointXYZRGB>::Ptr colored_cloud = reg.getColoredCloud ();
    //pcl::visualization::CloudViewer viewer ("Cluster viewer");
    pcl::visualization::PCLVisualizer viewer("cluser viewer");
    //viewer.showCloud(colored_cloud);
    viewer.addPointCloud(colored_cloud, "segmented");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "segmented");
    //viewer.addCoordinateSystem (1.0);
    viewer.initCameraParameters ();
    //viewer.setSize(1920, 1080);
    //viewer.setCameraPosition(0, 0, 0, 0, 0, 1);

    while (!viewer.wasStopped ())
    {
      viewer.spinOnce (100);
      boost::this_thread::sleep (boost::posix_time::microseconds (100000));
    }
  }

  pcl::io::savePCDFile(output_path, *cloud_final);

  return (0);
}
