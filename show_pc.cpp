#include <iostream>
#include <vector>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/min_cut_segmentation.h>
#include <stdio.h>

#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/pcl_visualizer.h>

#include "util.h"

typedef pcl::PointXYZRGB CloudType;

int main (int argc, char** argv)
{
  if (argc < 2) {
    printf("please provide a pcd file");
    return -1;
  }
  pcl::PointCloud <CloudType>::Ptr cloud (new pcl::PointCloud <CloudType>);
  if ( pcl::io::loadPCDFile <CloudType> (argv[1], *cloud) == -1 )
  {
    std::cout << "Cloud reading failed." << std::endl;
    return (-1);
  }

  pcl::visualization::PCLVisualizer viewer("cluser viewer");
  //viewer.showCloud(colored_cloud);
  //viewer.addPointCloud(applyUniformColor(*cloud, 255, 255, 255), "segmented");
  viewer.addPointCloud(cloud, "segmented");
  viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "segmented");
  viewer.addCoordinateSystem (1.0);
  viewer.initCameraParameters ();
  //viewer.setSize(1920, 1080);
  //viewer.setCameraPosition(0, 0, 0, 0, 0, 1);
  while (!viewer.wasStopped ())
  {
    viewer.spinOnce (100);
    boost::this_thread::sleep (boost::posix_time::microseconds (100000));
  }

  return (0);
}
