#!/bin/bash

input_path=$1
output_path=$2

base_path=$(dirname $0)

tmp_path=/tmp/temporary_cloud.pcd

$base_path/../build/color_growth $input_path --distance 20 --region_color 4 \
  --point_color 4.5 --smooth 5 --curve 30 --output $tmp_path $input_path

$base_path/../build/region_growth --curve 80 --smooth 2.5 --normal 25 \
  --neighbor 100 --output $output_path $tmp_path

rm -f $tmp_path
