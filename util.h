#include <iostream>
#include <vector>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/min_cut_segmentation.h>
#include <stdio.h>

template <typename PointType >
pcl::PointCloud <pcl::PointXYZRGB>::Ptr applyUniformColor(
  pcl::PointCloud<PointType> & input, uint8_t red, uint8_t green,
  uint8_t blue
) {
  pcl::PointCloud <pcl::PointXYZRGB>::Ptr output(
    new pcl::PointCloud <pcl::PointXYZRGB>
  );
  uint32_t rgb = (
    static_cast<uint32_t>(red) << 16 | static_cast<uint32_t>(green) << 8
    | static_cast<uint32_t>(blue)
  );
  for (int i = 0; i < input.size(); i++) {
    PointType * input_point = &input[i];
    if (input_point->x != input_point->x) continue;
    pcl::PointXYZRGB point;
    point.x = input_point->x;
    point.y = input_point->y;
    point.z = input_point->z;
    //printf("%f\n", input_point->rgb);
    point.rgb = *reinterpret_cast<float*>(&rgb);
    output->push_back(point);
  }
  return output;
}
